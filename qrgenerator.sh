#!/bin/bash


if [ $# -eq 0 ]
  then
    cat << EndOfMessage
[GNU GPL 2 license] 
	SYNOPSIS
		./qrcodegenerator [folder] file 

	This script use a file as a source and each row is a text associated to a qrcode.
	The folder represents the place where the qrcode will be written.
	If no folder are specified, the qr-codes  will be written in the main script folder.
 	This script requires qrencode package that can be downloaded using apt in Debian-like system or using pacman.	
EndOfMessage
    exit 1
fi

I=0


if [ $# -eq 2 ]
  then
   QPATH=$1
   FREAD=$2
fi
if [ $# -eq 1 ]
   then
   QPATH=""
   FREAD=$1
fi

while read line           
do           
    qrencode -d 300 -l M -s 10 -t PNG -o $QPATH$I.png line 
    qrencode -l M -t SVG -o $QPATH$I.svg line   
    I=$((I+1))
done < $FREAD

